# office插件

#### 介绍
适用场景：Word(*.docx)、Excel(*.xlsx)、Pdf

#### 引用方法（pom.xml）

```
<dependency>
	<groupId>cn.javaex</groupId>
	<artifactId>office</artifactId>
	<version>3.1.2</version>
</dependency>

<!-- 如果是基于spring的web项目，则下面2个依赖可以不用引入 -->
<dependency>
	<groupId>javax.servlet</groupId>
	<artifactId>javax.servlet-api</artifactId>
	<version>4.0.1</version>
	<scope>provided</scope>
</dependency>
<dependency>
	<groupId>org.springframework</groupId>
	<artifactId>spring-webmvc</artifactId>
	<version>4.3.7.RELEASE</version>
	<scope>provided</scope>
</dependency>
```


#### 插件文档地址

[http://doc.javaex.cn/office](http://doc.javaex.cn/office)


#### 官网
[http://www.javaex.cn](http://www.javaex.cn)
